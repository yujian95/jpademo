### 使用步骤 
1. 修改db.properties

将用户名,密码,url改成自己的.

2. 创建数据表

使用`src/main/resources/empSys.sql`,创建mysql.


### 可能的错误及解决方案

- 配置环境为:`jdk 8` 如果版本大于1.8的时候,(例如jdk10 , 测试的时候会报 empService.findEmps() `NullPointerException`)

- 此外,还有报一个 `java.lang.ClassNotFoundException: javax.xml.bind.JAXBException`

    >JAXB API是java EE 的API，因此在java SE 9.0 中不再包含这个 Jar 包。java 9 中引入了模块的概念，默认情况下，Java SE中将不再包含java EE 的Jar包,而在 java 6/7 / 8 时关于这个API 都是捆绑在一起的.

    - 解决方案改回jdk 1.8
    - 添加如下依赖.

```xml
<dependencies>
    <dependency>
        <groupId>javax.xml.bind</groupId>
        <artifactId>jaxb-api</artifactId>
        <version>2.3.0</version>
    </dependency>
    <dependency>
        <groupId>com.sun.xml.bind</groupId>
        <artifactId>jaxb-impl</artifactId>
        <version>2.3.0</version>
    </dependency>
    <dependency>
        <groupId>com.sun.xml.bind</groupId>
        <artifactId>jaxb-core</artifactId>
        <version>2.3.0</version>
    </dependency>
    <dependency>
        <groupId>javax.activation</groupId>
        <artifactId>activation</artifactId>
        <version>1.1.1</version>
    </dependency>
</dependencies>

```
