package cn.yujian95.jpademo.dao;

import cn.yujian95.jpademo.entity.Emp;
import org.springframework.data.repository.CrudRepository;

/**
 * CrudRepository<Emp,Integer>：提供普通的增、删、查、改方法
 * PagingAndSortingRepository;提供了分页查询的方法
 * JpaRepository<Emp,Integer>：提供了条件和分页的方法，结合了PagingAndSortingRepository<T, ID>和QueryByExampleExecutor<T>两个接口的方法
 */

public interface EmpDao extends CrudRepository<Emp, Integer> {
}