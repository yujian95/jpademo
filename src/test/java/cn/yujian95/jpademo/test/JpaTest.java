package cn.yujian95.jpademo.test;


import cn.yujian95.jpademo.entity.Emp;
import cn.yujian95.jpademo.services.EmpService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JpaTest {
    /**
     * 测试查询数据
     */
    @Test
    public void testQuery() {
        ApplicationContext context = new ClassPathXmlApplicationContext("jpabeans.xml");
        EmpService empService = (EmpService) context.getBean("empService");

        Iterable<Emp> emps = empService.findEmps();

        for (Emp emp : emps) {
            System.out.println(emp);  // 输出测试
        }
    }

}
