# jpademo
A JPA demo base on jdk1.8.

[中文说明](https://github.com/YuJian95/jpademo/edit/master/README_CN.md)

### how to use it

1. fix the `/src/main/resources/db.properties` 

use your username,password,dbURL.

```
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.url=jdbc:mysql://xxx
jdbc.user=root
jdbc.password=root
```

2. create the db table by `/src/main/resources/empSys.sql`


### tips

may has those error

- empService.findEmps() `NullPointerException`

This is because you are using the environment is greater than JDK 1.8, JDK1.8 only needs to be changed on the line.

- java.lang.ClassNotFoundException: javax.xml.bind.JAXBException

This is because up of JDK 1.8 isn't container this Java EE jar.

And you can use the JDK 1.8 or Add these dependency.

```xml
<dependencies>
    <dependency>
        <groupId>javax.xml.bind</groupId>
        <artifactId>jaxb-api</artifactId>
        <version>2.3.0</version>
    </dependency>
    <dependency>
        <groupId>com.sun.xml.bind</groupId>
        <artifactId>jaxb-impl</artifactId>
        <version>2.3.0</version>
    </dependency>
    <dependency>
        <groupId>com.sun.xml.bind</groupId>
        <artifactId>jaxb-core</artifactId>
        <version>2.3.0</version>
    </dependency>
    <dependency>
        <groupId>javax.activation</groupId>
        <artifactId>activation</artifactId>
        <version>1.1.1</version>
    </dependency>
</dependencies>
```
