package cn.yujian95.jpademo.services;

import cn.yujian95.jpademo.dao.EmpDao;
import cn.yujian95.jpademo.entity.Emp;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class EmpService {
    @Resource
    private EmpDao empDao;

    public Iterable<Emp> findEmps() {
        return empDao.findAll();
    }
}
